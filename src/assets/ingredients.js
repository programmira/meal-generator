const ingredients = [
  {
    "ingredient": "Beans (red or black)",
    "included": 1
  },
  {
    "ingredient": "Bread",
    "included": 1
  },
  {
    "ingredient": "Broccoli",
    "included": 1
  },
  {
    "ingredient": "Cabbage",
    "included": 1
  },
  {
    "ingredient": "Carrots",
    "included": 1
  },
  {
    "ingredient": "Cauliflower",
    "included": 1
  },
  {
    "ingredient": "Chickpeas",
    "included": 1
  },
  {
    "ingredient": "Corn",
    "included": 1
  },
  {
    "ingredient": "Eggplant",
    "included": 1
  },
  {
    "ingredient": "Flour",
    "included": 1
  },
  {
    "ingredient": "Garlic",
    "included": 1
  },
  {
    "ingredient": "Lentils",
    "included": 1
  },
  {
    "ingredient": "Mushrooms",
    "included": 1
  },
  {
    "ingredient": "Mustard",
    "included": 1
  },
  {
    "ingredient": "Nuts",
    "included": 1
  },
  {
    "ingredient": "Onion",
    "included": 1
  },
  {
    "ingredient": "Pasta",
    "included": 1
  },
  {
    "ingredient": "Peppers (sweet)",
    "included": 1
  },
  {
    "ingredient": "Pine nuts",
    "included": 1
  },
  {
    "ingredient": "Potato",
    "included": 1
  },
  {
    "ingredient": "Pumpkin",
    "included": 1
  },
  {
    "ingredient": "Rice",
    "included": 1
  },
  {
    "ingredient": "Salad",
    "included": 1
  },
  {
    "ingredient": "Spinach",
    "included": 1
  },
  {
    "ingredient": "Spring onion",
    "included": 1
  },
  {
    "ingredient": "Sweet potato",
    "included": 1
  },
  {
    "ingredient": "Tofu",
    "included": 1
  },
  {
    "ingredient": "Tomato",
    "included": 1
  },
  {
    "ingredient": "(Vegan) cream cheese",
    "included": 1
  },
  {
    "ingredient": "Yoghurt",
    "included": 1
  },
  {
    "ingredient": "Zucchini",
    "included": 1
  }
]

export default ingredients
