import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import ingredients from "@/assets/ingredients.js";
export default new Vuex.Store({
  state: {
    ingredients: ingredients,
  },
  getters: {
    n_available_ingredients: state => {
      let n_available_ingredients = 0;
      var i;
      for (i = 0; i < state.ingredients.length; i++) {
        if (state.ingredients[i].included == 1) {
          n_available_ingredients = n_available_ingredients += 1;
        }
      }
      return n_available_ingredients;
    }
  },
  mutations: {
    check(state, i) {
      state.ingredients[i].included = state.ingredients[i].included ? 0 : 1;
    },
    add(state, new_ingredient) {
      state.ingredients.push({ ingredient: new_ingredient, included: 1 });
    },
    set_to_0(state, ingredient) {
      let j = 0;
      for (j = 0; j < state.ingredients.length; j++) {
        if (state.ingredients[j].ingredient == ingredient) {
          state.ingredients[j].included = 0;
        }
      }
    }
  },
})
